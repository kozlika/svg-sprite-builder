//
// SVG
// ----------------------------------------------------------------------------

'use strict';

const gulp         = require('gulp');
const svgSprite    = require('gulp-svg-sprite');
const plumber      = require('gulp-plumber');

// configs
// ----------------------------------------------------------------------------

var iconConfig = {
  shape                 : {
    dimension           : {
      maxWidth          : 32,
      maxHeight         : 32
    },
    id                  : {
      generator         :"i_%s"
    },
    transform           : ['svgo'],
    dest                : 'icon/svg'
  },
  mode                  : {
    symbol              : {
      render            : {
        css             : false,
        scss            : false,
      },
      dest              : ".",
      sprite            : "icon/sprite.svg",
      example           : {
        "dest"          : "icon/icon-sprite.html"
      }
    },
  },
  svg                   : {
    xmlDeclaration      : false,
    doctypeDeclaration  : false,
    dimensionAttributes : false
  }
};

var illusConfig = {
  shape                 : {
    dimension           : {
      maxWidth          : 64,
      maxHeight         : 64
    },
    transform           : ['svgo'],
    dest                : 'illus/svg'
  },
  mode                  : {
    symbol              : {
      render            : {
        css             : false,
        scss            : false,
      },
      dest              : ".",
      sprite            : "illus/sprite.svg",
      example           : {
        "dest"          : "illus/illus-sprite.html"
      }
    },
  },
  svg                   : {
    xmlDeclaration      : false,
    doctypeDeclaration  : false,
    dimensionAttributes : false
  }
};


// prep:sprite
// Fabrication du sprite svg
// ----------------------------------------------------------------------------
// Si les icones n'ont pas déjà été passées toutes en noir à la prépa,
// nettoyer le sprite avec chercher remplacer Sublime expression régulière
// fill="#[0-9A-F]{6}"

// icon-sprite
gulp.task('icon-sprite', () => {
  return gulp.src('**/*.svg', {cwd: 'source/icons'})
    .pipe(plumber())
    .pipe(svgSprite(iconConfig))
    .pipe(gulp.dest('dist'));
});

gulp.task('illus-sprite', () => {
  return gulp.src('**/*.svg', {cwd: 'source/illus'})
    .pipe(plumber())
    .pipe(svgSprite(illusConfig))
    .pipe(gulp.dest('dist'));
});

gulp.task('default', gulp.parallel('icon-sprite', 'illus-sprite'));
